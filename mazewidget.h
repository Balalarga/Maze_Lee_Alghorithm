#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPainter>
#include <QMouseEvent>
#include <QQueue>
#include <QTimer>

class Maze: public QWidget
{
    struct Point{
        int x,y;
    };
    Q_OBJECT

public:
    Maze(QWidget *parent = nullptr);
    ~Maze();

protected:
    void paintEvent(QPaintEvent* e);
    void paintGrid(QPainter& p);
    void paintField(QPainter& p);
    void mouseMoveEvent(QMouseEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void waveAlg();
    bool inField(int x, int y);

    QVector<QVector<int>> field;
    QQueue<Point> q;
    QTimer* timer;
    int mode;
    Point startPoint;
    Point endPoint;
    int count;
    bool clearMode;

public slots:
    void start();
    void startBtn();
    void stop();
    void clear();
    void save();
    void load();
    void modeChange(QString name);
    void changeSize(int n);

signals:
    void changeSpinSize(int n);
};

#endif // WIDGET_H
